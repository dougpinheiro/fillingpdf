package dougpinheiro.pdf.fillingpdf;

import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.TextField;

public class PDFManager {
	
	public static void main(String[] args) {
		PdfReader reader;
		try {
			
			reader = new PdfReader("D:/DOCUMENTOS_DOUGLAS/Trabalhos/Fillable PDF/Wedding Teste.pdf");
	        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream("D:/DOCUMENTOS_DOUGLAS/Trabalhos/Fillable PDF/Wedding Teste2.pdf"));
	        PdfWriter writer = stamper.getWriter();
	        
	        PdfFormField personal = PdfFormField.createEmpty(writer);
	        personal.setFieldName("personal");
	      
	        TextField name = new TextField(writer, new Rectangle(425, 2886, 485, 2906), "name");
	        name.setVisibility(TextField.VISIBLE_BUT_DOES_NOT_PRINT);
	        name.setFontSize(12);
	       /* final BaseFont bf = BaseFont.createFont(
	        	    "c:/windows/fonts/ARLRDBD.TTF",
	        	    BaseFont.IDENTITY_V, 
	        	    BaseFont.EMBEDDED
	        	); */
	        name.setFont(BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1250, false));
	        name.setTextColor(BaseColor.WHITE);
	        PdfFormField personal_name = name.getTextField();
	        personal.addKid(personal_name);
	        
	        stamper.addAnnotation(personal, 1);
	        stamper.close();
	        reader.close();
	        
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
